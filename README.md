# SG Demo Scene

> You can try a game via [link](http://gkzr.me/)

An app with 3 part:

1. Create 144 sprites (NOT graphics object) that are stacked on each other like cards in a deck(so object above covers bottom one, but not completely). Every second 1 object from top of stack goes to other stack - animation of moving should be 2 seconds long. So at the end of whole process you should have reversed stack. Display number of fps in left top corner and make sure, that this demo runs well on mobile devices.
2. Create a tool that will allow mixed text and images in an easy way (for example displaying text with emoticons or prices with money icon). It should come up every 2 seconds a random text with images in random configuration (image + text + image, image + image + image, image + image + text, text + image + text etc) and a random font size.
3. Particles - make a demo that shows an awesome fire effect. Please keep number of images low (max 10 sprites on screen at once). Feel free to use existing libraries how you would use them in a real project.

## Navigation

Upon launch the app is in Cards part.

To change the part swipe or click on the icon in the top.

## Cards

Cards is created one my one and them they shuffle. There is no limit for the shuffle,
it will stop only when the user will leave the part.

## Texts

The text are generated every 2 seconds with the random compilation of images and texts,
as well as the image and the texts are generated randomly.

## Fire

The fire is a simple implementation of the pixi-particles based on the example Flame.

# Structure

Due to time limitation the structure is as simple as possible.

The App class creates a pixi application and builds the base view:

* Background
* FPS meter
* Menu
* Scenes (Cards, Fire, Texter)
* Tooltip

Also it creates Swiping class and handle swipes to trigger menu API.

All views are basically pixi objects, inherited mostly from PIXI.Container.

# How to start

First install the dependencies

````
npm install
````

Second run build command

````
npm run build
````

This will create a working build in dist folder. Use any http server,
like [http-server](https://www.npmjs.com/package/http-server) to run it.
