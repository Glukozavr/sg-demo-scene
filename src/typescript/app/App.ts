import {Cards} from "./cards/Cards";
import {FPSMeter} from "./fps/FPSMeter";
import {Texter} from "./texter/Texter";
import {Fire} from "./fire/Fire";
import {Menu} from "./menu/Menu";
import {Listener} from "./observer/Events";
import {Swiping} from "./ui/Swiping";

export interface Options {
    height?: number;
    width?: number;
    antialias?: boolean;
}

export interface Updatable {
    update(dt: number): void;
}

export interface Scene extends PIXI.Container {
    start(): void;
    stop(): void;
    getTooltip(): string;
}

export class App implements Listener {

    public element: HTMLElement;
    public options: Options;
    public app: PIXI.Application;
    public fpsMeter: FPSMeter;
    public menu: Menu;
    public tooltip: PIXI.Text;
    public scenes: Scene[];
    public currentScene: Scene;

    protected lastTime: number;
    protected updatable: Updatable[] = [];
    protected swiping: Swiping;

    protected layout = {
        menu: {
            y: 300,
        },
        fpsMeter: {
            y: 40,
        },
    };

    protected defaultOptions: Options = {
        height: 256,
        width: 256,
        antialias: true,
    };

    constructor(element: HTMLElement, options?: Options) {
        this.element = element;

        this.options = {
            ...this.defaultOptions,
            ...options,
        };

        this.swiping = new Swiping();
        this.swiping.events.addListener(this);
    }

    public preload(images: Array<{name: string, url: string}>, handleProgress?: (count: number) => void) {
        let count = 0;

        return new Promise((resolve) => {
            PIXI.loader
                .add(images)
                .load(() => {
                    resolve();
                })
                .onProgress.add(() => {
                    count++;

                    if (typeof handleProgress !== "undefined") {
                        handleProgress(Math.round(count / (images.length - 1) * 100));
                    }
                });
        });
    }

    public initAnimation() {
        const app = this.app = new PIXI.Application(this.options);
        app.view.className = "main";

        const menu = this.menu = new Menu();
        menu.events.addListener(this);
        const fpsMeter = this.fpsMeter = new FPSMeter();
        this.scenes = [
            new Cards(),
            new Texter(),
            new Fire(),
        ];
        this.scenes.forEach((scene) => {
            if ("update" in scene) {
                this.updatable.push(scene);
            }
            scene.position.set(this.options.width / 2, this.options.height / 2 - this.layout.menu.y);
        });
        const tooltip = this.tooltip = new PIXI.Text("Tooltip");
        tooltip.style.fontSize = "60px";
        tooltip.style.fill = "#ffffff";

        const background = new PIXI.Sprite(PIXI.loader.resources.background.texture);
        background.anchor.set(0.5);
        background.position.set(this.options.width / 2, this.options.height / 2);

        menu.position.set(this.options.width / 2, this.layout.menu.y);
        fpsMeter.position.set(this.options.width / 2, this.layout.fpsMeter.y);
        this.updatable.push(fpsMeter);

        app.stage.addChild(background);
        app.stage.addChild(menu);
        app.stage.addChild(fpsMeter);
        app.stage.addChild(tooltip);

        this.element.appendChild(app.view);
    }

    public handle(event: string, data: any) {
        switch (event) {
            case "activation":
                this.activate();
                break;
            case "swipeLeft":
                this.menu.next();
                break;
            case "swipeRight":
                this.menu.previous();
                break;
        }
    }

    public async start() {
        this.startLoop();

        this.activate();
    }

    public resize() {
        if (this.app) {
            this.app.view.width = window.innerWidth;
            this.app.view.height = window.innerHeight;

            const scaling = window.innerHeight / this.options.height;
            this.app.stage.scale.set(scaling);
            const ratio = this.options.width / this.options.height;
            this.app.renderer.resize(window.innerHeight * ratio, window.innerHeight);
        }
    }

    protected activate() {
        if (this.currentScene) {
            this.currentScene.stop();
            this.app.stage.removeChild(this.currentScene);
        }
        const scene = this.scenes[this.menu.getActive()];
        this.app.stage.addChild(scene);
        scene.start();
        this.tooltip.text = scene.getTooltip();
        this.tooltip.position.set(this.options.width / 2 - this.tooltip.width / 2, this.options.height - 120);
        this.currentScene = scene;
    }

    protected startLoop() {
        this.lastTime = Date.now();

        const step = () => {
            const time = Date.now();
            const dt = time - this.lastTime;

            this.updatable.forEach((entity) => { entity.update(dt); });

            this.lastTime = time;
            window.requestAnimationFrame(step);
        };

        window.requestAnimationFrame(step);
    }

}
