import {Deck} from "./Deck";
import {MovableSprite} from "../ui/MovableSprite";
import {Scene} from "../App";

export class Cards extends PIXI.Container implements Scene {

    protected active = false;
    protected props = {
        textureName: "card",
        deckCardsNumber: 144,
        cardsNumber: 9,
        cardChangeAnimationDuration: 2,
        cardChangeDelay: 1000,
        showDuration: 2000,
    };
    protected size = {
        width: 1000,
        height: 1400,
    };
    protected shift: {x: number, y: number};
    protected deck: Deck;
    protected cards: MovableSprite[];

    constructor() {
        super();

        this.evaluateShift();
        this.createDeck();
        this.createCards();
    }

    async setCards() {
        if (this.deck.getLength() > 0) {
            return ;
        }

        for (const card of this.cards) {
            this.deck.push(card);

            await new Promise((resolve) => {
                setTimeout(() => resolve(), this.props.showDuration / this.props.deckCardsNumber);
            });
        }
    }

    start() {
        this.active = true;

        this.shuffle();
    }

    stop() {
        this.active = false;
    }

    getTooltip(): string {
        return "Swipe or click icons on top";
    }

    protected async shuffle() {
        await this.setCards();

        while (this.active) {
            const card = this.deck.pop();

            await this.deck.moveIn(card, this.props.cardChangeAnimationDuration);

            await new Promise((resolve) => {
                setTimeout(() => resolve(), this.props.cardChangeDelay);
            });
        }
    }

    protected createCards() {
        if (this.cards) { return; }

        this.cards = [];
        const textures = PIXI.loader.resources;
        for (let i = 0; i < this.props.deckCardsNumber; i++) {
            // Random card number
            const j = Math.round(Math.random() * (this.props.cardsNumber - 1));
            // Create sprite based on texture from the preloaded images
            const card = new MovableSprite(textures[`card${("" + j).length > 1 ? j : `0${j}`}`].texture);

            this.cards.push(card);
        }
    }

    protected evaluateShift() {
        const card = PIXI.loader.resources.card00.texture;

        this.shift = {
            x: (this.size.width - card.width) / this.props.deckCardsNumber,
            y: (this.size.height - card.height) / this.props.deckCardsNumber,
        };
    }

    protected createDeck() {
        this.deck = new Deck(this.shift);
        this.deck.position.set(0, this.size.height);
        this.addChild(this.deck);
    }

}
