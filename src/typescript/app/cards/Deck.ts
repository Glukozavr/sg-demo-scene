import {MovableSprite} from "../ui/MovableSprite";

export class Deck extends PIXI.Container {

    protected size = {
        x: 1000,
        y: 1000,
    };
    protected shift: {x: number, y: number};
    protected cards: MovableSprite[] = [];

    constructor(shift: Deck["shift"]) {
        super();

        this.shift = shift;
    }

    public getLength() {
        return this.cards.length;
    }

    public clean() {
        this.cards = [];
    }

    public push(card: MovableSprite) {
        const position = this.getPositionForIndex(this.cards.length);

        card.position.set(position.x, position.y);
        this.cards.push(this.addChild(card));
    }

    public pop(): MovableSprite {
        return this.cards.pop();
    }

    public moveIn(card: MovableSprite, duration: number) {
        const promises: Array<Promise<any>> = [];

        this.addChildAt(card, this.children.length);
        this.cards.unshift(card);

        this.cards.forEach((c, index) => {
            const position = this.getPositionForIndex(index);
            promises.push(
                c.moveTo(position.x, position.y, duration)
                    .then(() => { this.addChildAt(c, index); }),
            );
        });

        return Promise.all(promises);
    }

    protected getPositionForIndex(index: number) {
        return {
            x: index * this.shift.x - this.size.x / 2,
            y: -index * this.shift.y - this.size.y / 2,
        };
    }
}
