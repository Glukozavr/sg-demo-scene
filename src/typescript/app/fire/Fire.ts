import {Scene, Updatable} from "../App";
import {Howl} from "howler";
import {SOUNDS} from "../preload";

export class Fire extends PIXI.Container implements Updatable, Scene {

    protected emitter: any;
    protected sound: Howl;

    constructor() {
        super();

        this.sound = new Howl({
            src: [SOUNDS.fireSound],
            loop: true,
            volume: 0.2,
        });
    }

    getTooltip(): string {
        return "It's just fires";
    }

    start() {
        this.sound.play();
        this.emitter = new (PIXI.particles as any).Emitter(
            this,
            [PIXI.loader.resources.fire.texture, PIXI.loader.resources.particle.texture],
            {
                alpha: {
                    start: 0.62,
                    end: 0,
                },
                scale: {
                    start: 1,
                    end: 2,
                },
                color: {
                    start: "fff191",
                    end: "ff622c",
                },
                speed: {
                    start: 500,
                    end: 500,
                },
                startRotation: {
                    min: 265,
                    max: 275,
                },
                rotationSpeed: {
                    min: 50,
                    max: 50,
                },
                lifetime: {
                    min: 0.1,
                    max: 0.75,
                },
                blendMode: "normal",
                frequency: 0.001,
                emitterLifetime: 0,
                maxParticles: 1000,
                pos: {
                    x: 0,
                    y: 500,
                },
                addAtBack: false,
                spawnType: "circle",
                spawnCircle: {
                    x: 0,
                    y: 0,
                    r: 10,
                },
            });
    }

    stop() {
        this.sound.stop();
        this.emitter.destroy();
        this.emitter = null;
    }

    update(dt: number) {
        if (this.emitter) {
            this.emitter.update(dt / 1000);
        }
    }

}
