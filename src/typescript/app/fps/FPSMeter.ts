import {Updatable} from "../App";

export class FPSMeter extends PIXI.Container implements Updatable {

    protected FPS: number;
    protected text: PIXI.Text;
    protected fps: number;
    protected counter: number;
    protected lastUpdateTime: number;

    constructor() {
        super();

        this.text = new PIXI.Text("FPS Meter");
        this.text.style.fill = "#ffffff";
        this.text.style.fontSize = "60px";
        this.text.position.set(-this.text.width / 2, 0);
        this.addChild(this.text);
        this.fps = 0;
        this.counter = 0;
        this.lastUpdateTime = Date.now();
    }

    public update(dt: number): void {
        const time = Date.now();
        this.fps += 1000 / (dt);
        this.counter++;

        if (time - this.lastUpdateTime > 1000) {
            this.FPS = Math.round(this.fps / this.counter);
            this.fps = 0;
            this.counter = 0;
            this.lastUpdateTime = time;

            if (this.FPS > 0 && this.FPS < Infinity) {
                this.text.text = `FPS: ${this.FPS}`;
                this.text.position.set(-this.text.width / 2, 0);
            }
        }
    }

}
