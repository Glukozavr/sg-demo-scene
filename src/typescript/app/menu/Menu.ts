import {MovableSprite} from "../ui/MovableSprite";
import {Events} from "../observer/Events";

export class Menu extends PIXI.Container {

    public events: Events;

    protected animationDuration = 0.5;
    protected icons: MovableSprite[];
    protected active: number;
    protected shift = 200;

    constructor() {
        super();

        this.events = new Events();
        this.createIcons();
        this.setPositions();
    }

    public getActive() {
        return this.active;
    }

    public next() {
        if (this.goTo(this.active + 1)) {
            return this.moveToPositions();
        }
    }

    public previous() {
        if (this.goTo(this.active - 1)) {
            return this.moveToPositions();
        }
    }

    protected goTo(index: number) {
        if (this.active === index) {
            return false;
        } else {
            this.active = index >= this.icons.length ? this.icons.length - 1 : index < 0 ? 0 : index;
            return true;
        }
    }

    protected createIcons() {
        this.icons = [
            this.addChild(new MovableSprite(PIXI.loader.resources.iconCards.texture)),
            this.addChild(new MovableSprite(PIXI.loader.resources.iconPaper.texture)),
            this.addChild(new MovableSprite(PIXI.loader.resources.iconFire.texture)),
        ];

        this.icons.forEach((icon, index) => {
            icon.anchor.set(0.5);
            icon.interactive = true;
            icon.on("pointerdown", () => {
                if (this.active !== index) {
                    this.goTo(index);
                    this.moveToPositions();
                }
            });
        });
        this.active = 0;
    }

    protected setPositions() {

        this.icons.forEach((icon, index) => {
            const props = this.getPropsForIndex(index);

            icon.position.set(props.position.x, props.position.y);
            icon.scale.set(props.scale.x, props.scale.y);
        });
    }

    protected moveToPositions() {
        const promises: Array<Promise<any>> = [];

        this.icons.forEach((icon, index) => {
            const props = this.getPropsForIndex(index);

            promises.push(icon.moveTo(props.position.x, props.position.y, this.animationDuration));
            promises.push(icon.scaleTo(props.scale.x, props.scale.y, this.animationDuration));
        });

        return Promise.all(promises)
            .then(() => {
                this.events.fire("activation", this.active);
            });
    }

    protected getPropsForIndex(index: number) {
        const xPos = (index - this.active) * this.shift;

        const position = {x: xPos, y: 0};
        const scaleNumber = index === this.active ? 1 : 0.5;
        const scale = {x: scaleNumber, y: scaleNumber};

        return {
            position,
            scale,
        };
    }

}
