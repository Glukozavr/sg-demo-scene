export interface Listener {
    handle(event: string, data: any): void;
}

export class Events {

    protected listeners: Listener[] = [];

    public addListener(listener: Listener) {
        if (!this.listeners.includes(listener)) {
            this.listeners.push(listener);
        }
    }

    public removeListener(listener: Listener) {
        if (this.listeners.includes(listener)) {
            this.listeners.splice(this.listeners.indexOf(listener), 1);
        }
    }

    public fire(event: string, data: any) {
        this.listeners.forEach((listener) => {
            listener.handle(event, data);
        });
    }

}
