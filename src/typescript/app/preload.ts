const IMAGES = {
    iconFire: "images/icon-fire.png",
    iconPaper: "images/icon-paper.png",
    iconCards: "images/icon-cards.png",
    background: "images/background.jpg",
    fire: "images/fire.png",
    particle: "images/particle.png",
    card00: "images/cards/tile000.png",
    card01: "images/cards/tile001.png",
    card02: "images/cards/tile002.png",
    card03: "images/cards/tile003.png",
    card04: "images/cards/tile004.png",
    card05: "images/cards/tile005.png",
    card06: "images/cards/tile006.png",
    card07: "images/cards/tile007.png",
    card08: "images/cards/tile008.png",
    emoji00: "images/emoji/tile000.png",
    emoji01: "images/emoji/tile001.png",
    emoji02: "images/emoji/tile002.png",
    emoji03: "images/emoji/tile003.png",
    emoji04: "images/emoji/tile004.png",
    emoji05: "images/emoji/tile005.png",
    emoji06: "images/emoji/tile006.png",
    emoji07: "images/emoji/tile007.png",
    emoji08: "images/emoji/tile008.png",
    emoji09: "images/emoji/tile009.png",
    emoji10: "images/emoji/tile010.png",
    emoji11: "images/emoji/tile011.png",
    emoji12: "images/emoji/tile012.png",
    emoji13: "images/emoji/tile013.png",
    emoji14: "images/emoji/tile014.png",
    emoji15: "images/emoji/tile015.png",
    emoji16: "images/emoji/tile016.png",
    emoji17: "images/emoji/tile017.png",
    emoji18: "images/emoji/tile018.png",
    emoji19: "images/emoji/tile019.png",
    emoji20: "images/emoji/tile020.png",
    emoji21: "images/emoji/tile021.png",
    emoji22: "images/emoji/tile022.png",
    emoji23: "images/emoji/tile023.png",
    emoji24: "images/emoji/tile024.png",
    emoji25: "images/emoji/tile025.png",
    emoji26: "images/emoji/tile026.png",
    emoji27: "images/emoji/tile027.png",
    emoji28: "images/emoji/tile028.png",
    emoji29: "images/emoji/tile029.png",
};

const SOUNDS = {
    fireSound: "sounds/fire-sound.mp3",
};

// Small conversion to fit into pixi loader
const PRELOADING = Object.keys(IMAGES).map((key: string) => { return {
    name: key,
    url: (IMAGES as {[key: string]: string})[key],
}; });

export {
    SOUNDS,
    PRELOADING,
    IMAGES,
};
