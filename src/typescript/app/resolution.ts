// Resolution configuration for the app
const RESOLUTION = {
    width: 1080,
    height: 1920,
    ratio: 1,
};

export {
    RESOLUTION,
};
