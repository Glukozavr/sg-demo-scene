export class SpecialText extends PIXI.Container {

    protected textsCache: PIXI.Text[] = [];
    protected currentPosition = 0;

    constructor() {
        super();
    }

    public getWidth() {
        return this.currentPosition;
    }

    createText(texts: Array<string | PIXI.Sprite>, fontSize: number) {
        while (this.children.length > 0) {
            const child = this.children.pop();
            this.removeChild(child);
        }

        this.currentPosition = 0;

        texts.forEach((value) => {
            if (typeof value === "string") {
                if (this.textsCache.length === 0) {
                    this.textsCache.push(new PIXI.Text());
                }
                const text = this.textsCache.pop();
                text.text = value;
                this.addText(text, fontSize);
            } else if (value instanceof PIXI.Sprite) {
                this.addImage(value, fontSize);
            }
        });
    }

    addText(text: PIXI.Text, fontSize: number) {
        text.position.set(this.currentPosition, 0);
        text.style.fontSize = `${fontSize}px`;
        text.style.fill = "#ffffff";
        text.visible = true;
        this.addChild(text);
        this.currentPosition += text.width;
    }

    addImage(sprite: PIXI.Sprite, fontSize: number) {
        sprite.position.set(this.currentPosition, 0);
        const ratio = sprite.height / sprite.width;
        sprite.height = fontSize;
        sprite.width = fontSize * ratio;
        sprite.visible = true;
        this.addChild(sprite);
        this.currentPosition += sprite.width;
    }
}
