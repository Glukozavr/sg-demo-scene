import {SpecialText} from "./SpecialText";
import {Scene} from "../App";

export enum TYPE {
    IMAGE,
    TEXT,
}

export class Texter extends PIXI.Container implements Scene {

    public specialText: SpecialText;

    protected active = false;
    protected images: {[id: string]: PIXI.Sprite} = {};
    protected props = {
        emojisCount: 30,
    };
    protected characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    constructor() {
        super();

        this.specialText = new SpecialText();
        this.addChild(this.specialText);
    }

    async start() {
        this.active = true;

        this.roll();
    }

    stop() {
        this.active = false;
    }

    getTooltip(): string {
        return "Text updated every 2 sec";
    }

    protected async roll() {
        while (this.active) {
            this.createRandomText();

            await new Promise((resolve) => { setTimeout(resolve, 2000); });
        }
    }

    protected createRandomText() {
        const elements: Array<string | PIXI.Sprite> = [];
        const elementsCount = 3;

        for (let i = 0; i < elementsCount; i++) {
            const type = Math.random() > 0.25 ? TYPE.IMAGE : TYPE.TEXT;

            switch (type) {
                case TYPE.IMAGE:
                    elements.push(this.getRandomImage());
                    break;
                case TYPE.TEXT:
                    const charCount = Math.random() * 6;
                    let text = "";
                    for (let k = 0; k < charCount; k++) {
                        text += this.characters[Math.round(Math.random() * (this.characters.length - 1))];
                    }
                    elements.push(text);
                    break;
            }
        }

        this.specialText.createText(elements, Math.round(12 + Math.random() * 200));
        this.specialText.position.set(-this.specialText.getWidth() / 2, 0);
    }

    protected getRandomImage() {
        const j = Math.round(Math.random() * (this.props.emojisCount - 1));
        const imageName = `emoji${("" + j).length > 1 ? j : `0${j}`}`;

        if (!this.images[imageName]) {
            this.images[imageName] = new PIXI.Sprite(PIXI.loader.resources[imageName].texture);
        }

        return this.images[imageName];
    }

}
