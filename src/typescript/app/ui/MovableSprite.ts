import gsap from "gsap";

export class MovableSprite extends PIXI.Sprite {

    constructor(texture?: PIXI.Texture) {
        super(texture);
    }

    moveTo(x: number, y: number, duration: number) {
        return new Promise((resolve) => {
            gsap.to(this.position, {
                duration,
                x,
                y,
                onComplete: resolve,
            });
        });
    }

    scaleTo(x: number, y: number, duration: number) {
        return new Promise((resolve) => {
            gsap.to(this.scale, {
                duration,
                x,
                y,
                onComplete: resolve,
            });
        });
    }

}
