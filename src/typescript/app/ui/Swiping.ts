import {Events} from "../observer/Events";

export class Swiping {

    public events: Events;
    public sensitivity = 50;

    protected initialPoint: {x: number, y: number};
    protected finalPoint: {x: number, y: number};

    constructor() {
        this.events = new Events();

        window.addEventListener("pointerdown", this.handlePointerDown.bind(this));
        window.addEventListener("pointerup", this.handlePointerUp.bind(this));
    }

    handlePointerDown(event: PointerEvent) {
        this.initialPoint = {
            x: event.clientX,
            y: event.clientY,
        };
    }

    handlePointerUp(event: PointerEvent) {
        this.finalPoint = {x: event.clientX, y: event.clientY};

        const xAbs = Math.abs(this.initialPoint.x - this.finalPoint.x);
        const yAbs = Math.abs(this.initialPoint.y - this.finalPoint.y);

        if (xAbs > this.sensitivity || yAbs > this.sensitivity) {
            if (xAbs > yAbs) {
                if (this.finalPoint.x < this.initialPoint.x) {
                    this.events.fire("swipeLeft", this);
                } else {
                    this.events.fire("swipeRight", this);
                }
            } else {
                if (this.finalPoint.y < this.initialPoint.y) {
                    this.events.fire("swipeUp", this);
                } else {
                    this.events.fire("swipeDown", this);
                }
            }
        }
    }

}
