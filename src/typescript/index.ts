// A starting point of the App
import {App} from "./app/App";
import {PRELOADING} from "./app/preload";
import {RESOLUTION} from "./app/resolution";

// Launch on page ready
window.addEventListener("load", function() {
    const app = new App(document.body, {
        height: RESOLUTION.height,
        width: RESOLUTION.width,
    });

    // Start with preloading the assets
    app.preload(PRELOADING)
        .then(() => {
            app.initAnimation();

            // Fix the positioning
            app.resize();
            app.start();
        });

    // Subscribe to window size change
    window.addEventListener("resize", app.resize.bind(app));
}, false);
