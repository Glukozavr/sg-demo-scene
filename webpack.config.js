const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const bundleFilename = "bundle.js";
module.exports = {
    entry: './src/typescript/index.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: bundleFilename
    },
    resolve: {
        extensions: ['.ts']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: false,
            bundle: bundleFilename,
            title: "SG Demo Scene"
        }),
        new CopyWebpackPlugin([
            {from: "./node_modules/pixi.js/dist/", to: ""},
            {from: "./node_modules/pixi-particles/dist/", to: ""},
            {from: "./src/style", to: "style/"},
            {from: "./src/assets/images", to: "images/"},
            {from: "./src/assets/sounds", to: "sounds/"}
            ])
    ]
};
